
Description

This repository contains the necessary configuration and scripts to upload Gatling performance test results to Gatling Enterprise using Maven.

Prerequisites

Java Development Kit (JDK): Ensure JDK is installed and configured properly on your machine.

Apache Maven: Make sure Maven is installed and configured correctly.

Gatling Enterprise Account: You need access to a Gatling Enterprise instance where you can upload your performance test results.

Configuration
Clone this repository to your local machine:
git clone <repository_url>

Execute this command from the directory
mvn gatling:enterpriseStart
To start the test on galting cloud


A simple showcase of a Maven project using the Gatling plugin for Maven. Refer to the plugin documentation
[on the Gatling website](https://gatling.io/docs/current/extensions/maven_plugin/) for usage.

It includes:

* [Maven Wrapper](https://maven.apache.org/wrapper/), so that you can immediately run Maven with `./mvnw` without having
  to install it on your computer
* minimal `pom.xml`
* latest version of `io.gatling:gatling-maven-plugin` applied
* sample [Simulation](https://gatling.io/docs/gatling/reference/current/general/concepts/#simulation) class,
  demonstrating sufficient Gatling functionality
* proper source file layout

