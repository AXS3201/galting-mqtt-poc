package simulations

import com.fasterxml.jackson.databind.ObjectMapper
import io.gatling.core.Predef._
import io.gatling.mqtt.Predef.{mqtt, _}

import java.io.ByteArrayInputStream
import java.util.zip.GZIPInputStream
import scala.concurrent.duration._
import scala.util.Using

class TestMqttSimulation extends Simulation {

  private val objectMapper = new ObjectMapper()
  val numberOfUsers = Integer.getInteger("users", 1)

  private def extractTransactionIdFromGzipBytes(bytes: Array[Byte]): String = {
    Using.resource(new GZIPInputStream(new ByteArrayInputStream(bytes))) { is =>
      objectMapper.readTree(is).get("_transaction_id").asText()
    }
  }

  val userFeederLong = jsonFile("data/TestDevices.json").circular
  val userFeederShort = jsonFile("data/TestDevicesMin.json").circular
  val feeder = if (numberOfUsers.gt(7000)) userFeederLong else userFeederShort

  private val mqttProtocol = mqtt
    .broker("api.perf-tdome.yumconnect.dev", 8883)
    .useTls(true)
    .cleanSession(false)
    .connectTimeout(300)
    .keepAlive(240)
    .reconnectDelay(240)
    .credentials("#{username}", "#{password}")

  private val topic = s"1/tb_us/#{store_number}/menu.gz"

  private val topicpublish = s"1/ack/#{id}"

  private val scn = scenario("Mqtt Scenario")
    .feed(feeder)
    .exec(mqtt("Connecting").connect,
      exec(
        doIf(session => session("username").as[String].endsWith("-1"))(
        mqtt("Subscribing").subscribe(topic).qosAtLeastOnce
          .await(3.minutes)
          .check(bodyBytes.transform(extractTransactionIdFromGzipBytes).saveAs("transactionId")))
        .exec { session =>
          session
        }.exec(doIf(session => session.contains("transactionId"))(
          mqtt("Publishing").publish(topicpublish)
            .message(StringBody("""{"_transaction_id": "#{transactionId}"}"""))
        ))
      )
    )

  setUp(
    scn.inject(atOnceUsers(numberOfUsers))
  ).maxDuration(10.minutes)
    .protocols(mqttProtocol)
}

